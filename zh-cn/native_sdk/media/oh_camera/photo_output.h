/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup OH_Camera
 * @{
 *
 * @brief 为相机模块提供C接口的定义。
 *
 * @syscap SystemCapability.Multimedia.Camera.Core
 *
 * @since 11
 * @version 1.0
 */

/**
 * @file photo_output.h
 *
 * @brief 声明拍照输出概念。
 *
 * @library libohcamera.so
 * @syscap SystemCapability.Multimedia.Camera.Core
 * @since 11
 * @version 1.0
 */

#ifndef NATIVE_INCLUDE_CAMERA_PHOTOOUTPUT_H
#define NATIVE_INCLUDE_CAMERA_PHOTOOUTPUT_H

#include <stdint.h>
#include <stdio.h>
#include "camera.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 拍照输出对象
 *
 * 可以使用{@link OH_CameraManager_CreatePhotoOutput}方法创建指针。
 *
 * @since 11
 * @version 1.0
 */
typedef struct Camera_PhotoOutput Camera_PhotoOutput;

/**
 * @brief 在{@link PhotoOutput_Callbacks}中被调用的拍照输出帧启动回调。
 *
 * @param photoOutput 传递回调的{@link Camera_PhotoOutput}。
 * @since 11
 */
typedef void (*OH_PhotoOutput_OnFrameStart)(Camera_PhotoOutput* photoOutput);

/**
 * @brief 在{@link PhotoOutput_Callbacks}中被调用的拍照输出帧快门回调。
 *
 * @param photoOutput 传递回调的{@link Camera_PhotoOutput}。
 * @param info 回调传递的{@link Camera_FrameShutterInfo}。
 * @since 11
 */
typedef void (*OH_PhotoOutput_OnFrameShutter)(Camera_PhotoOutput* photoOutput, Camera_FrameShutterInfo* info);

/**
 * @brief 在{@link PhotoOutput_Callbacks}中被调用的拍照输出帧结束回调。
 *
 * @param photoOutput 传递回调的{@link Camera_PhotoOutput}。
 * @param frameCount 回调传递的帧计数。
 * @since 11
 */
typedef void (*OH_PhotoOutput_OnFrameEnd)(Camera_PhotoOutput* photoOutput, int32_t frameCount);

/**
 * @brief 在{@link PhotoOutput_Callbacks}中被调用的拍照输出错误回调。
 *
 * @param photoOutput 传递回调的{@link Camera_PhotoOutput}。
 * @param errorCode 拍照输出的{@link Camera_ErrorCode}。
 *
 * @see CAMERA_SERVICE_FATAL_ERROR
 * @since 11
 */
typedef void (*OH_PhotoOutput_OnError)(Camera_PhotoOutput* photoOutput, Camera_ErrorCode errorCode);

/**
 * @brief 拍照输出的回调。
 *
 * @see OH_PhotoOutput_RegisterCallback
 * @since 11
 * @version 1.0
 */
typedef struct PhotoOutput_Callbacks {
    /**
     * 拍照输出帧启动事件。
     */
    OH_PhotoOutput_OnFrameStart onFrameStart;

    /**
     * 拍照输出框快门事件。
     */
    OH_PhotoOutput_OnFrameShutter onFrameShutter;

    /**
     * 拍照输出帧结束事件。
     */
    OH_PhotoOutput_OnFrameEnd onFrameEnd;

    /**
     * 拍照输出错误事件。
     */
    OH_PhotoOutput_OnError onError;
} PhotoOutput_Callbacks;

/**
 * @brief 注册拍照输出更改事件回调。
 *
 * @param photoOutput {@link Camera_PhotoOutput}实例。
 * @param callback 要注册的{@link PhotoOutput_Callbacks}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 11
 */
Camera_ErrorCode OH_PhotoOutput_RegisterCallback(Camera_PhotoOutput* photoOutput, PhotoOutput_Callbacks* callback);

/**
 * @brief 注销拍照输出更改事件回调。
 *
 * @param photoOutput {@link Camera_PhotoOutput}实例。
 * @param callback 要注销的{@link PhotoOutput_Callbacks}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 * @since 11
 */
Camera_ErrorCode OH_PhotoOutput_UnregisterCallback(Camera_PhotoOutput* photoOutput, PhotoOutput_Callbacks* callback);

/**
 * @brief 拍摄照片。
 *
 * @param photoOutput 用于捕获拍照的{@link Camera_PhotoOutput}实例。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_RUNNING}如果捕获会话未运行。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 11
 */
Camera_ErrorCode OH_PhotoOutput_Capture(Camera_PhotoOutput* photoOutput);

/**
 * @brief 使用捕获设置捕获拍照。
 *
 * @param photoOutput 用于捕获拍照的{@link Camera_PhotoOutput}实例。
 * @param setting 用于捕获拍照的{@link Camera_PhotoCaptureSetting}。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SESSION_NOT_RUNNING}如果捕获会话未运行。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 11
 */
Camera_ErrorCode OH_PhotoOutput_Capture_WithCaptureSetting(Camera_PhotoOutput* photoOutput,
    Camera_PhotoCaptureSetting setting);

/**
 * @brief 释放拍照输出。
 *
 * @param photoOutput 要释放的{@link Camera_PhotoOutput}实例。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 11
 */
Camera_ErrorCode OH_PhotoOutput_Release(Camera_PhotoOutput* photoOutput);

/**
 * @brief 检查是否支持镜像拍照。
 *
 * @param photoOutput {@link Camera_PhotoOutput}实例，用于检查是否支持镜像。
 * @param isSupported 是否支持镜像的结果。
 * @return {@link#CAMERA_OK}如果方法调用成功。
 *         {@link#INVALID_ARGUMENT}如果参数丢失或参数类型不正确。
 *         {@link#CAMERA_SERVICE_FATAL_ERROR}如果相机服务出现致命错误。
 * @since 11
 */
Camera_ErrorCode OH_PhotoOutput_IsMirrorSupported(Camera_PhotoOutput* photoOutput, bool* isSupported);

#ifdef __cplusplus
}
#endif

#endif // NATIVE_INCLUDE_CAMERA_PHOTOOUTPUT_H
/** @} */
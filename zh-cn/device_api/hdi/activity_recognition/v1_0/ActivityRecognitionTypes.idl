/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiActivityRecognition
 * @{
 *
 * @brief 提供订阅和获取用户行为的API
 *
 * MSDP（Multimodal Sensor Data Platform）可以获取行为识别驱动程序的对象或代理，然后调用该对象或代理提供的API，
 * 获取设备支持的行为类型，订阅或取消订阅不同的行为事件，获取当前的行为事件，以及获取设备缓存的行为事件。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file ActivityRecognitionTypes.idl
 *
 * @brief 定义行为识别模块使用的数据类型。
 *
 * 模块包路径：ohos.hdi.activity_recognition.v1_0
 * 
 * @since 3.2
 * @version 1.0
 */

package ohos.hdi.activity_recognition.v1_0;

/**
 * @brief 枚举行为事件的类型。
 *
 * 支持的行为包括：车载、骑车、步行、跑步、静止、快走、高铁、未知、电梯、相对静止、手持步行、躺卧、乘机、地铁等。
 *
 * @since 3.2
 */
enum ActRecognitionEventType {
    /** 进入某一行为 */
    ACT_RECOGNITION_EVENT_ENTER = 0x01,
    /** 退出某一行为 */
    ACT_RECOGNITION_EVENT_EXIT = 0x02,
};

/**
 * @brief 枚举功耗模式的类型。
 *
 * @since 3.2
 */
enum ActRecognitionPowerMode {
    /** 普通模式，不论主核是否休眠都会进行行为事件的上报。 */
    ACT_RECOGNITION_NORMAL_MODE = 0,
    /** 低功耗模式，主核休眠时，不会进行行为事件的上报。 */
    ACT_RECOGNITION_LOW_POWER_MODE = 1,
};

/**
 * @brief 定义行为事件上报的数据结构。
 *
 * @since 3.2
 */
struct ActRecognitionEvent {
    /** 行为类型，详见{@Link ActRecognitionEventType} */
    int activity;
    /** 事件类型，详见{@Link ActRecognitionEventType} */
    int eventType;
    /** 时间戳 */
    long timestamp;
    /** 置信度，100为最可信，0为最不可信，-1为该行为不支持置信度的计算。 */
    int confidence;
};
/** @} */
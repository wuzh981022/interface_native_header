/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Display
 * @{
 *
 * @brief 显示模块驱动接口定义。
 *
 * 提供给上层图形服务使用的驱动接口，包括图层管理、设备控制、显示内存管理等相关接口。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IRefreshCallback.idl
 *
 * @brief 显示刷新事件回调接口声明。
 *
 * 模块包路径：ohos.hdi.display.composer.v1_0
 *
 * 引用：ohos.hdi.display.composer.v1_0.DisplayComposerType
 *
 * @since 3.2
 * @version 1.0
 */

package ohos.hdi.display.composer.v1_0;

import ohos.hdi.display.composer.v1_0.DisplayComposerType;

/**
 * @brief 定义显示刷新事件回调接口。
 *
 * @since 3.2
 * @version 1.0
 */
[callback] interface IRefreshCallback {
    /**
     * @brief 显示刷新事件回调接口声明。
     *
     * @param devId 显示设备的ID。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link DispErrCode}。
     * @since 3.2
     * @version 1.0
     */
    OnRefresh([in] unsigned int devId);
}
/** @} */
/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Camera
 * @{
 *
 * @brief Camera模块接口定义。
 *
 * Camera模块涉及相机设备的操作、流的操作、离线流的操作和各种回调等。
 *
 * @since 4.0
 * @version 1.1
 */

/**
 * @file ICameraHost.idl
 *
 * @brief Camera服务的管理类，对上层提供HDI接口。
 *
 * 模块包路径：ohos.hdi.camera.v1_1
 *
 * 引用：
 * - ohos.hdi.camera.v1_0.ICameraDeviceCallback
 * - ohos.hdi.camera.v1_0.ICameraHost
 * - ohos.hdi.camera.v1_1.ICameraDevice
 * - ohos.hdi.camera.v1_1.Types
 *
 * @since 4.0
 * @version 1.1
 */

package ohos.hdi.camera.v1_1;

import ohos.hdi.camera.v1_0.ICameraDeviceCallback;
import ohos.hdi.camera.v1_0.ICameraHost;
import ohos.hdi.camera.v1_1.ICameraDevice;
import ohos.hdi.camera.v1_1.Types;

/**
 * @brief 定义Camera设备功能操作。
 *
 * 打开和预启动Camera设备的相关操作。
 * 
 * @since 4.0
 * @version 1.1
 */
interface ICameraHost extends ohos.hdi.camera.v1_0.ICameraHost {
    /**
     * @brief 打开Camera设备。
     *
     * 打开指定的Camera设备，通过此接口可以获取到ICameraDevice对象，通过ICameraDevice对象可以操作具体的Camera设备。
     *
     * @param cameraId 需要打开的Camera设备ID，可通过{@link GetCameraIds}接口获取当前已有Camera设备列表。
     * @param callbackObj Camera设备相关的回调函数，具体参见{@link ICameraDeviceCallback}。
     * @param device 返回当前要打开的Camera设备ID对应的ICameraDevice对象。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @see GetCameraIds
     *
     * @since 4.0
     * @version 1.1
     */
    OpenCamera_V1_1([in] String cameraId, [in] ICameraDeviceCallback callbackObj, [out] ICameraDevice device);

    /**
     * @brief 预启动Camera设备。
     *
     * 当需要加速cameraId指定Camera设备的启动时可调用该接口。
     *
     * @param config 表示预启动配置信息，当前可忽略。更多细节查看{@link PrelaunchConfig}。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 4.0
     * @version 1.1
     */
    Prelaunch([in] struct PrelaunchConfig config);
}
/** @} */
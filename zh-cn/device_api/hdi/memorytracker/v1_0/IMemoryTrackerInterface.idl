/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup MemoryTracker
 * @{
 *
 * @brief 实现对设备（如GPU）内存占用的统一查询，如GPU占用的GL和Graphic内存等。
 *
 * 需要查询GPU等外设内存占用时使用，例如hidumper中使用本模块IMemoryTrackerInterface接口列出每个进程的GPU内存占用。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IMemoryTrackerInterface.idl
 *
 * @brief 包含IMemoryTrackerInterface接口的声明、各项参数及返回值的意义。
 *
 * 需要查询外设内存占用时使用，通过该文件中定义的接口，获取指定类型的设备内存信息。
 *
 * 模块包路径：ohos.hdi.memorytracker.v1_0
 *
 * 引用：ohos.hdi.memorytracker.v1_0.MemoryTrackerTypes
 *
 * @since 3.2
 * @version 1.0
 */

package ohos.hdi.memorytracker.v1_0;

import ohos.hdi.memorytracker.v1_0.MemoryTrackerTypes;

/**
 * @brief 用于获取指定类型的设备内存信息的接口。
 *
 * 需要查询GPU等外设内存占用时使用，例如hidumper中使用本接口列出每个进程的GPU内存占用。
 *
 * @since 3.2
 */
interface IMemoryTrackerInterface {
    /**
     * @brief 获取指定类型的设备内存信息。
     *
     * @param pid表示进程的id，若pid为0则表示获取所有进程的内存记录。
     * @param type表示内存类型。 
     * @param records表示内存记录列表。
     *
     * @return 若操作成功，返回值为<b>0</b>。
     * @return 若操作失败，返回值为负值。
     *
     * @since 3.2
     */
    GetDevMem([in] int pid, [in] enum MemoryTrackerType type, [out] struct MemoryRecord[] records);
}
/** @} */
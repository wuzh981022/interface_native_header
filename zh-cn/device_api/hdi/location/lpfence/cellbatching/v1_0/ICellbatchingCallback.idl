/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup HdiLpfenceCellbatching
 * @{
 *
 * @brief 为低功耗围栏服务提供基站轨迹数据记录的API。
 *
 * 本模块能够控制设备对接收的基站数据进行缓存和上报。
 *
 * 应用场景：根据设备接收的基站轨迹数据，判断用户的大致活动区域，进而进行一些服务提醒。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @file ICellbatchingCallback.idl
 *
 * @brief 定义基站轨迹数据记录模块回调接口。
 *
 * 在用户订阅基站轨迹数据记录功能时需要注册这个回调函数接口的实例。
 *
 * 模块包路径：ohos.hdi.location.lpfence.cellbatching.v1_0
 *
 * 引用：ohos.hdi.location.lpfence.cellbatching.v1_0.CellbatchingTypes
 *
 * @since 4.0
 * @version 1.0
 */

package ohos.hdi.location.lpfence.cellbatching.v1_0;

import ohos.hdi.location.lpfence.cellbatching.v1_0.CellbatchingTypes;

/**
 * @brief 定义基站轨迹数据记录模块的回调函数。
 *
 * 用户在开启基站轨迹数据记录功能前，需要先注册该回调函数。当应用主动获取基站轨迹数据时，会通过回调函数进行上报。
 * 详情可参考{@link ICellbatchingInterface}。 
 *
 * @since 4.0
 */
[callback] interface ICellbatchingCallback {
    /**
     * @brief 定义基站轨迹数据上报的回调函数。
     *
     * 基站轨迹数据会通过该回调函数进行上报。
     *
     * @param data 上报的基站轨迹数据。详见{@link CellTrajectoryData}定义。
     *
     * @return 如果回调函数上报数据成功，则返回0。
     * @return 如果回调函数上报数据失败，则返回负值。
     *
     * @since 4.0
     */
    OnCellbatchingChanged([in] struct CellTrajectoryData[] data);

    /**
     * @brief 定义低功耗围栏服务复位事件通知的回调函数。
     *
     * 低功耗围栏服务发生复位时会通过该回调函数进行事件上报。
     *
     * @return 如果回调函数上报事件成功，则返回0。
     * @return 如果回调函数上报事件失败，则返回负值。
     *
     * @since 4.0
     */
    OnCellbatchingReset();
}
/** @} */
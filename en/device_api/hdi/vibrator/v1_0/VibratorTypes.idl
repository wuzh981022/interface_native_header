/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Vibrator
 * @{
 *
 * @brief Provides APIs for vibrator services to access the vibrator driver.
 *
 * After obtaining a driver object or agent, a vibrator service starts or stops the vibrator
 * using the APIs provided by the driver object or agent.
 *
 * @since 2.2
 * @version 1.0
 */

/**
 * @file VibratorTypes.idl
 *
 * @brief Declares the vibrator data structure, including the vibration mode and effect.
 *
 * @since 2.2
 * @version 1.0
 */

/**
 * @brief Defines the package path of the Vibrator module APIs.
 *
 * @since 2.2
 * @version 1.0
 */
package ohos.hdi.vibrator.v1_0;

/**
 * @brief Enumerates the vibration modes of this vibrator.
 *
 * @since 2.2
 */
enum HdfVibratorMode {
    HDF_VIBRATOR_MODE_ONCE,     /**< A one-shot vibration with the given duration. */
    HDF_VIBRATOR_MODE_PRESET,   /**< A periodic vibration with the preset effect. */
    HDF_VIBRATOR_MODE_BUTT,     /**< Invalid mode. */
};
/** @} */

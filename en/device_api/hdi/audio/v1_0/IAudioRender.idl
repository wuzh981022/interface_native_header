/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @brief Defines the package path of the audio APIs.
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.audio.v1_0;

/**
 * @addtogroup Audio
 * @{
 *
 * @brief Defines audio-related APIs, including data types and functions for loading drivers, accessing a driver adapter, and rendering and capturing audios.
 *
 *
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IAudioRender.idl
 *
 * @brief Declares APIs for audio rendering.
 *
 * @since 3.2
 * @version 1.0
 */

import ohos.hdi.audio.v1_0.AudioTypes;
import ohos.hdi.audio.v1_0.IAudioCallback;

/**
 * @brief Provides capabilities for audio rendering, including controlling the rendering, setting audio attributes, scenes, and volume, obtaining hardware latency, and rendering audio frames.
 *
 * 
 *
 * @since 3.2
 * @version 1.0
 */
interface IAudioRender {
    /**
     * @brief Obtains the estimated latency of the audio device driver.
     *
     * @param ms Indicates the latency obtained, in milliseconds.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    GetLatency([out] unsigned int ms);

    /**
     * @brief Writes a frame of output data (downlink data) into the audio driver for rendering.
     *
     * @param frame Indicates the frame to write.
     * @param replyBytes Indicates the actual length (in bytes) of the audio data to write.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    RenderFrame([in] byte[] frame, [out] unsigned long replyBytes);

    /**
     * @brief Obtains the number of output audio frames.
     *
     * @param frames Indicates the number of audio frames obtained. For details, see {@link AudioTimeStamp}.
     * @param time Indicates the timestamp associated with the frames.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @see RenderFrame
     *
     * @since 3.2
     * @version 1.0
     */
    GetRenderPosition([out] unsigned long frames, [out] struct AudioTimeStamp time);

    /**
     * @brief Sets the audio rendering speed.
     *
     * @param speed Indicates the rendering speed to set, for example, 0.5, 0.75, 1.0, 1.25, 1.5, and 2.0.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @see GetRenderSpeed
     *
     * @since 3.2
     * @version 1.0
     */
    SetRenderSpeed([in] float speed);

    /**
     * @brief Obtains the current audio rendering speed.
     *
     * @param speed Indicates the rendering speed obtained.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @see SetRenderSpeed
     *
     * @since 3.2
     * @version 1.0
     */
    GetRenderSpeed([out] float speed);

    /**
     * @brief Sets the channel mode for audio rendering.
     *
     * @param mode Indicates the channel mode to set. For details, see {@link AudioChannelMode}.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @see GetChannelMode
     *
     * @since 3.2
     * @version 1.0
     */
    SetChannelMode([in] enum AudioChannelMode mode);

    /**
     * @brief Obtains the current channel mode for audio rendering.
     *
     * @param mode Indicates the channel mode obtained. For details, see {@link AudioChannelMode}.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @see SetChannelMode
     *
     * @since 3.2
     * @version 1.0
     */
    GetChannelMode([out] enum AudioChannelMode mode);

    /**
     * @brief Registers an audio callback that will be invoked during playback when buffer data writing or buffer drain is complete.
     *
     * @param audioCallback Indicates the callback to register. For details, see {@link IAudioCallback}.
     * @param cookie Indicates the callback parameters.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @see RegCallback
     *
     * @since 3.2
     * @version 1.0
     */
    RegCallback([in] IAudioCallback audioCallback, [in] byte cookie);

    /**
     * @brief Drains the buffer.
     *
     * @param type Indicates the callback notification type. For details, see {@link AudioDrainNotifyType}.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @see RegCallback
     *
     * @since 3.2
     * @version 1.0
     */
    DrainBuffer([out] enum AudioDrainNotifyType type);

    /**
     * @brief Checks whether the function of draining the buffer is supported.
     *
     * @param support Specifies whether the function of draining the buffer is supported. The value <b>true</b> means that the function is supported, and <b>false</b> means the opposite.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    IsSupportsDrain([out] boolean support);
    /**
     * @brief Checks whether the configuration of an audio scene is supported.
     *
     * @param scene Indicates the descriptor of the audio scene. For details, see {@link AudioSceneDescriptor}.
     * @param supported Specifies whether the configuration is supported. The value <b>true</b> means that the configuration is supported, and <b>false</b> means the opposite.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @see SelectScene
     *
     * @since 3.2
     * @version 1.0
     */
    CheckSceneCapability([in] struct AudioSceneDescriptor scene, [out] boolean supported);

    /**
     * @brief Selects an audio scene.
     *
     * <ul>
     *   <li>1. To select a specific audio scene, you need to specify both the application scenario and output device. For example, to select a scene using a speaker as the output device, set <b>scene</b> according to the scenario where the speaker is used. For example:
     *     <ul>
     *       <li>For media playback, set <b>scene</b> to <b>media_speaker</b>. </li>
     *       <li>For a voice call, set <b>scene</b> to <b>voice_speaker</b>. </li>
     *     </ul>
     *   <li>2. To select only the application scenario, such as media playback, movie, or gaming, you can set <b>scene</b> to <b>media</b>, <b>movie</b>, or <b>game</b>, respectively. </li>
     *   <li>3. To select only the output device, such as media receiver, speaker, or headset, you can set <b>scene</b> to <b>receiver</b>, <b>speaker</b>, or <b>headset</b>, respectively. </li>
     * </ul>
     *
     * @param scene Indicates the descriptor of the audio scene. For details, see {@link AudioSceneDescriptor}.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @see CheckSceneCapability
     *
     * @since 3.2
     * @version 1.0
     */
    SelectScene([in] struct AudioSceneDescriptor scene);

    /**
     * @brief Mutes the audio.
     *
     * @param mute Specifies whether to mute the audio. The value <b>true</b> means to mute the audio, and <b>false</b> means the opposite.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @see GetMute
     *
     * @since 3.2
     * @version 1.0
     */
    SetMute([in] boolean mute);

    /**
     * @brief Checks whether the audio is muted.
     *
     * @param mute Specifies whether the audio is muted. The value <b>true</b> means that the audio is muted, and <b>false</b> means the opposite.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @see SetMute
     *
     * @since 3.2
     * @version 1.0
     */
    GetMute([out] boolean mute);

    /**
     * @brief Sets the audio volume.
     *
     * The volume ranges from 0.0 to 1.0. If the volume level in the audio service ranges from 0 to 15,
     * <b>0.0</b> indicates that the audio is muted, and <b>1.0</b> indicates the maximum volume level (15).
     *
     * @param volume Indicates the volume to set. The value ranges from 0.0 to 1.0.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    SetVolume([in] float volume);

    /**
     * @brief Obtains the audio volume.
     *
     * @param volume Indicates the volume obtained. The value ranges from 0.0 to 1.0.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @see SetVolume
     *
     * @since 3.2
     * @version 1.0
     */
    GetVolume([out] float volume);

    /**
     * @brief Obtains the range of the audio gain.
     *
     * The audio gain can be expressed in one of the following ways (depending on the chip platform), corresponding to two types of value ranges:
     *
     * <ul>
     *   <li>1. Actual audio gain values, for example, values ranging from –50 to 6 dB. </li>
     *   <li>2. Floating-point numbers ranging from 0.0 to 1.0, where
     *          <b>0.0</b> indicates to mute the input audio, and <b>1.0</b> indicates the maximum gain (6 dB).</li></ul> </li>
     * </ul>
     *
     * @param min Indicates the minimum value of the range.
     * @param max Indicates the maximum value of the range.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @see GetGain
     * @see SetGain
     *
     * @since 3.2
     * @version 1.0
     */
    GetGainThreshold([out] float min, [out] float max);

    /**
     * @brief Obtains the audio gain.
     *
     * @param gain Indicates the audio gain obtained.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @see GetGainThreshold
     * @see SetGain
     *
     * @since 3.2
     * @version 1.0
     */
    GetGain([out] float gain);

    /**
     * @brief Sets an audio gain.
     *
     * @param gain Indicates the audio gain to set. The value ranges from 0.0 to 1.0.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @see GetGainThreshold
     * @see GetGain
     *
     * @since 3.2
     * @version 1.0
     */
    SetGain([in] float gain);

    /**
     * @brief Obtains the size of an audio frame.
     *
     *
     *
     * @param size Indicates the audio frame size, in bytes.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    GetFrameSize([out] unsigned long size);

    /**
     * @brief Obtains the number of audio frames in the audio buffer.
     *
     * @param count Indicates the number of audio frames obtained.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    GetFrameCount([out] unsigned long count);

    /**
     * @brief Sets audio sampling attributes.
     *
     * @param attrs Indicates the audio sampling attributes to set, such as the sampling rate, sampling precision, and channel. For details, see {@link AudioSampleAttributes}.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @see GetSampleAttributes
     *
     * @since 3.2
     * @version 1.0
     */
    SetSampleAttributes([in] struct AudioSampleAttributes attrs);

    /**
     * @brief Obtains audio sampling attributes.
     *
     * @param attrs Indicates the audio sampling attributes obtained, such as the sampling rate, sampling precision, and channel.
     *              For details, see {@link AudioSampleAttributes}.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @see SetSampleAttributes
     *
     * @since 3.2
     * @version 1.0
     */
    GetSampleAttributes([out] struct AudioSampleAttributes attrs);

    /**
     * @brief Obtains the data channel ID.
     *
     * @param channelId Indicates the data channel ID obtained.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    GetCurrentChannelId([out] unsigned int channelId);

    /**
     * @brief Sets extended audio parameters.
     *
     * @param keyValueList Indicates the key-value list of the extended audio parameter. The format is <b>key=value</b>. Separate multiple key-value pairs by semicolons (;).
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    SetExtraParams([in] String keyValueList);

    /**
     * @brief Obtains the extended audio parameters.
     *
     * @param keyValueList Indicates the key-value list of the extended audio parameter. The format is <b>key=value</b>. Separate multiple key-value pairs by semicolons (;).
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    GetExtraParams([out] String keyValueList);

    /**
     * @brief Requests a mmap buffer.
     *
     * @param reqSize Indicates the size of the request mmap buffer.
     * @param desc Indicates the buffer descriptor. For details, see {@link AudioMmapBufferDescriptor}.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    ReqMmapBuffer([in] int reqSize, [in] struct AudioMmapBufferDescriptor desc);

    /**
     * @brief Obtains the read/write position of the current mmap buffer.
     *
     * @param frames Indicates the frame where the read/write starts.
     * @param time Indicates the timestamp obtained. For details, see {@link AudioTimeStamp}.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    GetMmapPosition([out] unsigned long frames, [out] struct AudioTimeStamp time);

    /**
     * @brief Adds an audio effect.
     *
     * @param effectid Indicates the ID of the audio effect to add.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    AddAudioEffect([in] unsigned long effectid);

    /**
     * @brief Removes an audio effect.
     *
     * @param effectid Indicates the ID of the audio effect to remove.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    RemoveAudioEffect([in] unsigned long effectid);

    /**
     * @brief Obtains the buffer size.
     *
     * @param bufferSize Indicates the buffer size obtained, in bytes.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    GetFrameBufferSize([out] unsigned long bufferSize);

    /**
     * @brief Starts audio rendering or capturing.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @see Stop
     *
     * @since 3.2
     * @version 1.0
     */
    Start();

    /**
     * @brief Stops audio rendering or capturing.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @see Start
     *
     * @since 3.2
     * @version 1.0
     */
    Stop();

    /**
     * @brief Pauses audio rendering or capturing.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @see Resume
     *
     * @since 3.2
     * @version 1.0
     */
    Pause();

    /**
     * @brief Resumes audio rendering or capturing.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @see Pause
     *
     * @since 3.2
     * @version 1.0
     */
    Resume();

    /**
     * @brief Flushes data in the audio buffer.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    Flush();

    /**
     * @brief Sets or cancels the standby mode of the audio device.
     *
     * @return Returns <b>0</b> if the device is set to standby mode; returns a positive value if the standby mode is canceled; returns a negative value if the setting or cancellation fails.
     *
     * @since 3.2
     * @version 1.0
     */
    TurnStandbyMode();

    /**
     * @brief Dumps information about the audio device.
     *
     * @param range Indicates the range of the device information to dump, which can be brief or full information.
     * @param fd Indicates the file to which the device information will be dumped.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    AudioDevDump([in] int range, [in] int fd);

    /**
     * @brief Checks whether the audio adapter supports pausing and resuming of audio rendering.
     *
     * @param supportPause Specifies whether pausing of audio rendering is supported. The value <b>true</b> means that pausing is supported, and <b>false</b> means the opposite.
     * @param supportResume Specifies whether resuming of audio rendering is supported. The value <b>true</b> means that resuming is supported, and <b>false</b> means the opposite.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    IsSupportsPauseAndResume([out] boolean supportPause, [out] boolean supportResume);
}
/** @} */
